#!/usr/bin/python
# -*- coding: utf-8 -*-
"""This file contains all the datasets functionalities."""
from __future__ import annotations
from typing import Iterator, Any, Dict, Union, List, Tuple

import os
import pickle
import struct
import types
import atexit
import select
import signal
import heapq
from collections import deque

from tqdm import trange, tqdm


class _BasePipe:
    _header = struct.Struct("!L")

    def __init__(self, fileno):
        self._fileno = fileno
        self._closed = False

    def fileno(self):
        return self._fileno

    def close(self):
        if not self._closed:
            os.close(self._fileno)
            self._closed = True

    def __del__(self):
        self.close()


class _PipeR(_BasePipe):
    def recv(self) -> Any:
        """Receive a pickled message over the given channel."""

        payload = b""
        while len(payload) < self._header.size:
            payload += os.read(self._fileno, self._header.size - len(payload))
        length = self._header.unpack(payload)[0]

        payload = b""
        while len(payload) < length:
            payload += os.read(self._fileno, length - len(payload))
        return pickle.loads(payload)


class _PipeW(_BasePipe):
    def send(self, obj: Any):
        payload = pickle.dumps(obj)
        os.write(self._fileno, self._header.pack(len(payload)) + payload)


def _pipe():
    r, w = os.pipe()
    return _PipeR(r), _PipeW(w)


def _create_pipes(n: int) -> Tuple[List[_PipeR], List[_PipeW]]:
    rs = []
    ws = []
    for _i in range(n):
        r, w = _pipe()
        rs.append(r)
        ws.append(w)
    return rs, ws


class Dataset:
    """
    # Dataset

    This class encapsulate a collection of samples.
    """

    data = None
    is_leaf: bool = None
    leaf_type: type = None

    def __init__(self, data: List[Any] = None):
        self.data = data

    def __iter__(self):
        return DatasetIterator(self)

    @classmethod
    def zip(cls, data: Dict[str, Union[List[Any], Dataset]]) -> Dataset:
        ds = cls()
        ds.data = {}
        for key, val in data.items():
            if isinstance(val, Dataset):
                ds.data[key] = val
            else:
                ds.data[key] = cls.from_raw_list(val)
        return ds

    @classmethod
    def from_raw_list(cls, data: List[Any]) -> "Dataset":
        return cls.from_raw_iterator(iter(data))

    @classmethod
    def from_raw_iterator(cls, it: Iterator[Dict[str, Any]]):
        ds = cls()
        for sample in it:
            ds.append(sample)
        return ds

    # @classmethod
    # def from_raw_iterator(cls, data: Iterator[Any]):

    #     # Extract the first element.
    #     first_sample = next(data)

    #     if isinstance(first_sample, dict):

    #         # Construct the structure data.
    #         ds = {}
    #         dq = deque([(first_sample, ds)])

    #         while len(dq) > 0:
    #             cur = dq.pop()
    #             for k, v in cur[0].items():
    #                 if isinstance(v, dict):
    #                     cur[1][k] = {}
    #                     dq.append((v, cur[1][k]))
    #                 else:
    #                     cur[1][k] = Dataset(data=[v])

    #         ds = Dataset.zip(ds)
    #         for sample in data:
    #             ds.append(sample)
    #         return ds
    #     else:
    #         return cls([first_sample] + list(data))

    def _append_dict(self, sample: Dict[str, Any]):
        if self.data is None:
            self.data = {}
            self.is_leaf = False
            for key, val in sample.items():
                self.data[key] = Dataset()
                self.data[key].append(val)
        else:
            dq = deque([(sample, self.data)])
            while len(dq) > 0:
                cur = dq.pop()
                for k, v in cur[0].items():
                    if isinstance(v, dict):
                        dq.append((v, cur[1][k]))
                    else:
                        cur[1][k].append(v)

    def append(self, sample):
        if isinstance(sample, dict):
            return self._append_dict(sample)
        if self.data is None:
            self.data = [sample]
            self.is_leaf = True
            self.leaf_type = type(sample)
        elif not self.is_leaf:
            raise TypeError("Sample to add must be a dictionary.")
        elif type(sample) != self.leaf_type:
            raise TypeError(f"Sample to add must be a {self.leaf_type}")
        else:
            self.data.append(sample)

    def __getitem__(
        self, key: Union[slice, int, str]
    ) -> Union[DatasetIterator, Dataset, Any]:

        if isinstance(key, slice):
            return DatasetIterator(self, _slice=key)

        if isinstance(key, str) and not self.is_leaf:
            return self.data[key]

        if isinstance(key, int):
            return self.at(key)

        if self.is_leaf:
            raise TypeError("Index must be an integer.")
        else:
            raise TypeError("Index must be either an integer or a string.")

    def __len__(self):
        if self.data is None:
            return 0
        if self.is_leaf:
            return len(self.data)
        else:
            lengths = {len(ds) for ds in self.data.values()}
            assert len(lengths) == 1, "All members MUST have the same length."
            return lengths.pop()

    def __repr__(self):
        keys_repr = ""
        if isinstance(self.data, dict):
            keys_repr = ' keys="' + ",".join(self.data.keys()) + '"'
        return f"<Dataset len={len(self)}{keys_repr} />"

    def at(self, idx: int):
        if self.is_leaf:
            return self.data[idx]
        else:
            return {k: ds.at(idx) for k, ds in self.data.items()}

    def map(self, f: callable, num_workers: int = None) -> Dataset:

        # Total of samples to process.
        total = len(self)

        # Estimate a good number of workers.
        if num_workers is None:
            num_workers = max(1, len(os.sched_getaffinity(0)) - 1)

        if num_workers == 1:

            def gen():
                for i in trange(total, desc="dataset.map"):
                    y = f(self.at(i))
                    if isinstance(y, types.GeneratorType):
                        yield from y
                    else:
                        yield y

            return type(self).from_raw_list(gen())

        pids = []

        def cleanup():
            for pid in pids:
                os.kill(pid, signal.SIGKILL)
            for pid in pids:
                os.waitid(os.P_PID, pid, os.WEXITED)

        atexit.register(cleanup)

        # Create some pipes for distribution.
        pipes_ddr, pipes_ddw = _create_pipes(num_workers)
        pipes_dcir, pipes_dciw = _create_pipes(num_workers)

        # Process to write the chunks.
        pid = os.fork()
        if pid == 0:
            atexit.unregister(cleanup)
            del pipes_ddr, pipes_dciw

            # Start simply putting a job on each worker.
            for i in range(min(num_workers, total)):
                pipes_ddw[i].send((i, self.at(i)))

            i += 1
            while i < total:
                for p in select.select(pipes_dcir, [], [])[0]:
                    # Wait for a message signaling that the worker
                    # is now free to receive extra job.
                    p.recv()

                    # We dont use this message at the moment.
                    j = pipes_dcir.index(p)
                    if i < total:
                        pipes_ddw[j].send((i, self.at(i)))
                        i += 1
                    else:
                        # Signal that we are closing the pipe and that an event
                        # detectable by select may occurr.
                        pipes_ddw[j].send((-1, None))
                        del pipes_ddw[j]
                        del pipes_dcir[j]

            # Read the last confirmations after finishing distribution.
            while len(pipes_dcir) > 0:
                for p in select.select(pipes_dcir, [], [])[0]:
                    p.recv()
                    j = pipes_dcir.index(p)
                    pipes_ddw[j].send((-1, None))
                    del pipes_ddw[j]
                    del pipes_dcir[j]
            os._exit(0)
        else:
            pids.append(pid)
            del pipes_ddw, pipes_dcir

        # At this point we have the pipes: ddr and dciw.
        # Create extra pipes for the content.
        pipes_rdr, pipes_rdw = _create_pipes(num_workers)

        # Create a condition to finish consuming from the queue.
        for n in range(num_workers):
            pid = os.fork()
            if pid == 0:
                atexit.unregister(cleanup)

                # Extract the pipes.
                pipe_rdw = pipes_rdw[n]
                pipe_ddr = pipes_ddr[n]
                pipe_dciw = pipes_dciw[n]
                del pipes_rdw, pipes_ddr, pipes_dciw, pipes_rdr

                # Use select.
                rlist = [pipe_ddr]
                while len(rlist) > 0:
                    for p in select.select(rlist, [], [])[0]:
                        i, x = p.recv()
                        if i < 0:
                            rlist = []
                            pipe_rdw.send((-1, None, None))
                            continue
                        y = f(x)
                        if isinstance(y, types.GeneratorType):
                            response = (i, x, list(y))
                        else:
                            response = (i, x, [y])
                        pipe_rdw.send(response)
                        pipe_dciw.send(1)
                os._exit(0)
            else:
                pids.append(pid)

        del pipes_dciw, pipes_ddr, pipes_rdw

        received = 0
        data = []
        next_i = 0
        results = []
        with tqdm(total=total, desc="dataset.map", smoothing=0.001) as bar:
            while len(pipes_rdr) > 0:
                for p in select.select(pipes_rdr, [], [])[0]:
                    res = p.recv()
                    if res[0] < 0:
                        pipes_rdr.remove(p)
                    else:
                        if res[0] == next_i:
                            data.extend(res[2])
                            next_i += 1
                        else:
                            heapq.heappush(results, res)

                        while len(results) > 0 and results[0][0] == next_i:
                            res = heapq.heappop(results)
                            data.extend(res[2])
                            next_i += 1

                        received += 1
                        bar.update(1)

        assert total == received, "BUG!"

        for pid in pids:
            os.waitid(os.P_PID, pid, os.WEXITED)

        atexit.unregister(cleanup)

        return type(self).from_raw_list(data)


class DatasetIterator:
    """
    # DatasetIterator

    An iterator over a dataset, a key difference with Dataset is that
    the iterator does not ensure to have a proper length.
    """

    ds: Dataset

    # Next item to return.
    _next_idx: int = None

    # Slicing information.
    _slice: slice = None

    def __init__(self, ds: Dataset, _slice: slice = None):

        # Keep a reference to the dataset.
        self.ds = ds

        # By default the starting index is zero and consume the whole dataset.
        self._next_idx = 0
        self._slice = slice(0, None, 1)

        if _slice is not None:
            if _slice.start is not None:
                self._next_idx = _slice.start
            self._slice = slice(self._next_idx, _slice.stop, _slice.step)

        # Force a step of one if none.
        if self._slice is not None and self._slice.step is None:
            self._slice = slice(self._slice.start, self._slice.stop, 1)

    def __iter__(self):
        _slice = slice(self._next_idx, self._slice.stop, self._slice.step)
        return DatasetIterator(self.ds, _slice)

    def __next__(self):
        if self._next_idx is None:
            raise StopIteration

        if self._slice is not None:

            # Check if next index is after the stop.
            if self._slice.stop is not None:
                if self._next_idx >= self._slice.stop:
                    raise StopIteration

            # Stop iteration if we finished the dataset.
            if self._next_idx >= len(self.ds):
                raise StopIteration

            # Get the element from the dataset.
            el = self.ds.at(self._next_idx)

            # Increase by a step.
            self._next_idx += self._slice.step

            return el

        raise StopIteration
