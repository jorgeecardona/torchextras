#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import coverage
import unittest
from unittest.mock import patch
from torchextras.datasets import Dataset


class TestDatasets(unittest.TestCase):
    def test_simple_usage_of_dataset(self):
        ds = Dataset()
        assert len(ds) == 0, "Empty dataset."
        for i in range(0, 10):
            ds.append(i)
            assert len(ds) == i + 1, f"{i} element(s) in the dataset."
            assert ds[i] == i, f"Element {i} must be {i}."

        ds = Dataset()
        ds.append({"x": 1})
        assert len(ds) == 1, "One element in the dataset."
        assert isinstance(ds["x"], Dataset), "Field 'x' must be a dataset."

        ds = Dataset()
        for i in range(10):
            ds.append(i)

        ds = Dataset.zip({"x": ds})
        assert len(ds) == 10, "Nested dataset length is preserved."
        assert len(ds["x"]) == 10, "Nested dataset length is preserved."

    def test_from_raw_list(self):
        ds = Dataset.from_raw_list([{"x": 1}, {"x": 2}])
        self.assertEqual(len(ds), 2)
        self.assertIsInstance(ds, Dataset)
        self.assertIsInstance(ds["x"], Dataset)
        self.assertEqual(ds["x"][1], 2)

    def tetst_from_nested_raw_list(self):
        ds = Dataset.from_raw_list([{"x": {"y": 1}, "z": 3}, {"x": {"y": 2}, "z": 4}])
        self.assertEqual(len(ds), 2)
        self.assertIsInstance(ds, Dataset)
        self.assertIsInstance(ds["x"], Dataset)
        self.assertIsInstance(ds["x"]["y"], Dataset)
        self.assertIsInstance(ds["z"], Dataset)
        self.assertEqual(ds["x"]["y"][1], 2)

    def test_simple_map(self):
        ds = Dataset.from_raw_iterator(range(10))
        ds = ds.map(lambda x: {"x": x}, num_workers=1)
        self.assertIsInstance(ds["x"], Dataset)
        for i in range(10):
            self.assertEqual(ds["x"][i], i)

    def test_map(self):

        _os_exit = os._exit

        def _exit(status):
            cov = coverage.Coverage.current()
            cov.stop()
            cov.save()
            _os_exit(status)

        with patch("torchextras.datasets.os._exit", new=_exit):
            ds = Dataset.from_raw_iterator(range(128))
            ds = ds.map(lambda x: {"x": x}, num_workers=16)
            self.assertIsInstance(ds["x"], Dataset)
            for i in range(10):
                self.assertEqual(ds["x"][i], i)

    def test_simple_map_generator(self):
        ds = Dataset.from_raw_iterator(range(10))

        def f(x):
            yield from [x] * x

        ds = ds.map(f, num_workers=1)
        self.assertEqual(len(ds), 9 * 5)

    def test_dataset_indexing(self):
        ds = Dataset.from_raw_iterator(range(10))
        self.assertEqual(ds[0], 0)
        with self.assertRaises(TypeError):
            ds[1.1]

        with self.assertRaises(TypeError):
            ds["x"]

        ds = Dataset.zip({"x": ds})
        self.assertEqual(ds["x"][0], 0)
        self.assertEqual(ds[0], {"x": 0})

    def test_dataset_iter_slice(self):
        ds = Dataset.from_raw_iterator(range(10))
        for i, x in enumerate(ds):
            self.assertEqual(x, i)

        for i, x in enumerate(ds[1:5], 1):
            self.assertEqual(x, i)
